'use strict'

dnode    = require 'dnode'
assert   = require 'assert'
Cache    = require 'async-cache'
Database = require('sqlite3').Database;
Crixalis = require('crixalis')

Speed = 10

db = new Database require('./package.json').name + '.sqlite3', ->
	@run '''
		CREATE TABLE IF NOT EXISTS data (
			key   text NOT NULL,
			value text
		)'''

	return

weather = new Cache
	max    : 200
	length : -> 1
	load   : (date, callback) ->
		dnode.connect 3110, '192.168.117.110', (remote, connection) ->
			remote.forecast date, (error, result) ->
				do ->
					azimuth   = null
					magnitude = null

					# Got error from remote
					if error
						callback(error, null)
						return

					# Validate data
					try
						azimuth   = +result.azimuth
						magnitude = +result.magnitude

						assert azimuth   >= 0,   'Azimuth has wrong value'
						assert azimuth   <= 360, 'Azimuth has wrong value'
						assert magnitude >= 0,   'Magnitude has wrong value'
						assert magnitude < 50,   'Magnitude has wrong value'
					catch error
						console.log("Wrong weather for #{date}")
						callback(error, null)
						return

					console.log("Got weather for #{date}: #{azimuth} #{magnitude}")

					# Save result
					callback null,
						azimuth   : azimuth
						magnitude : magnitude

					return

				connection.end()
				return

			return

cities = [
	{
		id       : 4
		absciss  : 10
		ordinate : 10
	},
	{
		id       : 5
		absciss  : 60
		ordinate : 10
	},
	{
		id       : 6
		absciss  : 60
		ordinate : 50
	}
]

citiesByID = {}
citiesByID[city.id] = city for city in cities

process = (order, callback) ->
	date = null
	from = null
	to   = null

	order = Object(order)

	# Validate input data
	try
		# All fields required
		for field in ['from', 'to', 'when']
			assert field of order, 'Bad request'

		# Only real cities allowed
		for id in [order.from, order.to]
			assert citiesByID[id], "Wrong city ID #{id}"

		from = citiesByID[order.from]
		to   = citiesByID[order.to]

		# Same city
		assert from isnt to, 'Same city'

		date = +Date.parse(order.when)

		# Date should be in future
		# assert date > Date.now(), 'Wrong date'
	catch error
		console.log('Rejecting order: ' + JSON.stringify(order))
		callback(error, null)
		return

	console.log('Placing order: ' + JSON.stringify(order))

	# Try to place order
	placeOrder(date, from, to, callback)

	return

placeOrder = (date, from, to, callback) ->
	Ax = from.absciss
	Ay = from.ordinate
	Bx = to.absciss
	By = to.ordinate

	# Get weather
	weather.get date, (error, result) ->
		return callback(error, null) if error

		distance = (x1, x2, y1, y2) -> Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2))
		startDistance = distance(Ax, Bx, Ay, By)
		azimuth = result.azimuth * Math.PI / 180

		Wx = result.magnitude * Math.sin(azimuth)
		Wy = result.magnitude * Math.cos(azimuth)

		afterOneSecond = distance(Ax + Wx, Bx, Ay + Wy, By)

		# We have enough speed to deliver
		deliverable = !!((afterOneSecond - startDistance) < Speed)
		status = ['impossible', 'deliverable'][+deliverable]

		console.log("#{startDistance} #{afterOneSecond}")
		console.log('Order is ' + status)

		db.run('INSERT INTO data (key, value) VALUES (?, ?)', status, JSON.stringify(
			weather: result
			order:
				to   : to
				from : from
				date : date
		), ->)

		callback(null, !!deliverable)
		return

Crixalis
	.plugin('shortcuts')
	.plugin('static')
	.plugin('compression')
	.router()
	.get '/data.json', ->
		@async = yes
		@view  = 'json'

		db.all 'SELECT key,value FROM data', (error, result) =>
			@stash.json =
				status: 'ok'
				data: result.map (item) ->
					data = JSON.parse(item.value)
					data.status = item.key
					return data

			@render()

			return
		return

Crixalis.staticPath = 'public'

Crixalis.start('http', 3000)

(server = dnode((remote, connection) -> @['try'] = process))
	.listen(8090)
